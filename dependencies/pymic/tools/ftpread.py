__author__ = 'olivier'
import urllib
from ftplib import FTP

def test_ftp():
    f = urllib.urlopen('ftp://lisaserver.ulb.ac.be/pymic_samples/test1.zvi')
    return f.read()

def open_file_from_ftp(host = 'lisaserver.ulb.ac.be', dir = 'pymic_samples', filename = 'test1.zvi'):
    f = urllib.urlopen('ftp://%(host)s/%(dir)s/%(filename)s'%{'host':host,'dir':dir,'filename':filename})
    return f

def demo_list():
    ftp = FTP('lisaserver.ulb.ac.be')   # connect to host, default port
    ftp.login()               # user anonymous, passwd anonymous@
    ftp.cwd('pymic_samples')
    ftp.retrlines('LIST')     # list directory contents

if __name__ == "__main__":

    demo_list()
    f = open_file_from_ftp(filename='exp0001.jpg')
    ima = f.read()
    print f
    print ima
    