import matplotlib

__author__ = 'olivier'

import numpy as np
import os

import matplotlib.cm as cm
import matplotlib.pyplot as plt

#definition of the red/green/blue palet from black to single channel maximum
_red = {'red': [(0.0,  0.0, 0.0),(1.0 ,  1.0, 1.0)],
     'green': [(0.0,  0.0, 0.0),(1.0,  0.0, 0.0)],
     'blue':  [(0.0,  0.0, 0.0),(1.0,  0.0, 0.0)]}
_green = {'red':[(0.0,  0.0, 0.0),(1.0,  0.0, 0.0)],
     'green': [(0.0,  0.0, 0.0),(1.0,  1.0, 1.0)],
     'blue':  [(0.0,  0.0, 0.0),(1.0,  0.0, 0.0)]}
_blue = {'red': [(0.0,  0.0, 0.0),(1.0,  0.0, 0.0)],
     'green': [(0.0,  0.0, 0.0),(1.0,  0.0, 0.0)],
     'blue':  [(0.0,  0.0, 0.0),(1.0,  1.0, 1.0)]}

red_cmap = matplotlib.colors.LinearSegmentedColormap('redmap', _red)
green_cmap = matplotlib.colors.LinearSegmentedColormap('greenmap', _green)
blue_cmap = matplotlib.colors.LinearSegmentedColormap('bluemap', _blue)

def combineRGB(red=None,green=None,blue=None):
    '''returns one RGB image using palet and 3 optional channels'''
    if red is not None:
        sh = red.shape
    if green is not None:
        sh = green.shape
    if blue is not None:
        sh = blue.shape

    rgb = np.zeros((sh[0], sh[1], 4))

    if red is not None:
        rgb = rgb + matplotlib.cm.ScalarMappable(cmap=red_cmap).to_rgba(red)
    if green is not None:
        rgb = rgb + matplotlib.cm.ScalarMappable(cmap=green_cmap).to_rgba(green)
    if blue is not None:
        rgb = rgb + matplotlib.cm.ScalarMappable(cmap=blue_cmap).to_rgba(blue)

    return rgb

if __name__ == "__main__":
    red,green = np.meshgrid(range(100),range(100))
    blue = red+green
    print red
    print green
    print blue

    rgb = combineRGB(red=red,green=green,blue=blue)
    print rgb
    plt.imshow(rgb)
    plt.show()
