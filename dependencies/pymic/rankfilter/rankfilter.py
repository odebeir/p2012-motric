# -*- coding: utf-8 -*-
'''
Test the scipy weave module to the filt function (rankfilter)
8 and 10 bit per pixel
use Weave for C compilation

.. code-block:: python

   from scipy import misc
   from rankfilter import rankfilter

   im = misc.imread('../../test/data/cameraman.tif')
   f = rankFilter(im,filtName = 'mean',radius = 30,verbose = False)
   misc.imsave('../../test/data/cameraman_filt_mean.tif',f)
   print f
'''
__author__ = 'olivier'

import numpy as npy
from scipy.weave import inline
from scipy import misc

def _dtype2ctype(array):
    """convert numpy type in C equivalent type
    """
    types = {
        npy.dtype(npy.float64): 'double',
        npy.dtype(npy.float32): 'float',
        npy.dtype(npy.int32): 'int',
        npy.dtype(npy.int16): 'short',
        npy.dtype(npy.uint8): 'unsigned char',
        npy.dtype(npy.uint16): 'unsigned short',
    }
    return types.get(array.dtype)

rankfiltercore = {}
rankfiltercore['mean'] = 'sum=0.0f;for(m=0;m<HISTOSIZE;m++)sum+=(m*histo[m]);*ptemp=sum/pop;'
rankfiltercore['meansubstraction'] = 'sum=0.0f;for(m=0;m<HISTOSIZE;m++)sum+=(m*histo[m]);*ptemp=(((float)*p-sum/pop)/2.0f+MEDIF);'
rankfiltercore['egalise'] = 'sum=0.0f;for(m=0;m<=*p;m++)sum+=histo[m];*ptemp=sum*MAXIF/pop;'
rankfiltercore['minimum'] = 'for(m=0;(m<HISTOSIZE)&&(histo[m]==0);m++);*ptemp=m;'
rankfiltercore['maximum'] = 'for(m=HISTOSIZE;(--m)&&(histo[m]==0););*ptemp=m;'
rankfiltercore['tophat'] = 'for(m=HISTOSIZE;(--m)&&(histo[m]==0););*ptemp=m-*p;'
rankfiltercore['bottomhat'] = 'for(m=0;(m<HISTOSIZE)&&(histo[m]==0);m++);*ptemp=*p-m;'
rankfiltercore['gradient'] = 'for(m=HISTOSIZE;(--m)&&(histo[m]==0);); max=m; for(m=0;(m<HISTOSIZE)&&(histo[m]==0);m++); min=m; *ptemp=max-min;'
rankfiltercore['morph_contr_enh'] = 'for(m=HISTOSIZE;(--m)&&(histo[m]==0);); max=m; for(m=0;(m<HISTOSIZE)&&(histo[m]==0);m++); min=m; if(abs(*p-min)<abs(*p-max)) *ptemp=min; else *ptemp=max;'
rankfiltercore['autolevel'] = 'for(m=HISTOSIZE;(--m)&&(histo[m]==0);); max=m;    for(m=0;(m<HISTOSIZE)&&(histo[m]==0);m++); min=m; if(max-min){*ptemp=((float)(*p-min)/(float)(max-min)*MAXIF);}else{*ptemp=(float)*p;}'
rankfiltercore['lowest'] = 'for(m=0;(m<HISTOSIZE)&&(histo[m]==0);m++); min=m; if(abs(*p-min)<=delta)*ptemp=MAXI;'
rankfiltercore['highest'] = 'for(m=HISTOSIZE;(--m)&&(histo[m]==0);); max=m; if(abs(*p-max)<=delta)*ptemp=MAXI;'
rankfiltercore['extrema'] = 'for(m=HISTOSIZE;(--m)&&(histo[m]==0);); max=m; for(m=0;(m<HISTOSIZE)&&(histo[m]==0);m++); min=m; if((abs(*p-min)<=delta)||(abs(*p-max)<=delta))*ptemp=MAXI;'
rankfiltercore['threshold'] = 'sum=0.0f;for(m=0;m<HISTOSIZE;m++)sum+=(m*histo[m]); if(*p+delta>sum/pop)*ptemp=MAXI;'
rankfiltercore['median_threshold'] = 'sum=0;for(m=0;m<HISTOSIZE;m++){sum+=(float)histo[m];if(sum>pop*0.5){if(*p+delta>m)*ptemp=MAXI;m=HISTOSIZE;}};'
rankfiltercore['rank'] = 'sum=0;for(m=0;m<HISTOSIZE;m++){sum+=(float)histo[m];if(sum>pop*rank){*ptemp=m;m=HISTOSIZE;}};'
rankfiltercore['median'] = 'sum=0;for(m=0;m<HISTOSIZE;m++){sum+=(float)histo[m];if(sum>=pop*.5){*ptemp=m;m=HISTOSIZE;}};'
rankfiltercore['modal'] = 'max=0;pmax=0;for(m=0;m<HISTOSIZE;m++){if(max<histo[m]){max=histo[m];pmax=m;}};*ptemp=pmax;'
rankfiltercore['soft_morph_contr_enh'] = 'sum=0;for(m=0;m<HISTOSIZE;m++){sum+=histo[m];if(sum>pop*inf){min=m; m=HISTOSIZE;}}; sum=0;for(m=0;m<HISTOSIZE;m++){sum+=histo[m];if(sum>pop*sup){max=m; m=HISTOSIZE;}} if(abs(*p-min)<abs(*p-max)) *ptemp=min;    else *ptemp=max;'
rankfiltercore['soft_autolevel'] = 'sum=0;for(m=0;m<HISTOSIZE;m++){sum+=histo[m];if(sum>pop*inf){min=m; m=HISTOSIZE;}};sum=0;for(m=0;m<HISTOSIZE;m++){sum+=histo[m];if(sum>pop*sup){max=m; m=HISTOSIZE;}};if(max-min){f=((float)(*p-min)*MAXIF)/(float)(max-min);    f=MAX(0,MIN(MAXIF,f)); *ptemp=f;}else{*ptemp=(float)*p;}'
rankfiltercore['soft_gradient'] = 'sum=0;for(m=0;m<HISTOSIZE;m++){sum+=histo[m];if(sum>pop*inf){min=m; m=HISTOSIZE;}};sum=0;for(m=0;m<HISTOSIZE;m++){sum+=histo[m];if(sum>pop*sup){max=m; m=HISTOSIZE;}};f=(float)(max-min);*ptemp=f;'
rankfiltercore['soft_mean'] = 'sum=0;for(m=0;m<HISTOSIZE;m++){sum+=histo[m];if(sum>pop*inf){min=m; m=HISTOSIZE;}};    sum=0;for(m=0;m<HISTOSIZE;m++){sum+=histo[m];if(sum>pop*sup){max=m; m=HISTOSIZE;}};sum=0;count=0;for(m=min;m<max;m++){sum+=(m*histo[m]);count+=histo[m];};if(pop){f=(float)(sum)/(float)count;}else{f=0;};*ptemp=f;'
rankfiltercore['soft_meansubstraction'] = 'sum=0;for(m=0;m<HISTOSIZE;m++){sum+=histo[m];if(sum>pop*inf){min=m; m=HISTOSIZE;}};sum=0;for(m=0;m<HISTOSIZE;m++){sum+=histo[m];if(sum>pop*sup){max=m; m=HISTOSIZE;}};sum=0;count=0;for(m=min;m<max;m++){sum+=(m*histo[m]);count+=histo[m];};if(pop){f=((float)(*p)-(float)(sum)/(float)count)/2.0f+MEDIF;}else{f=0;};*ptemp=f;'
#spectral kernels
rankfiltercore['spectral_mean'] = 'sga=MAX(0,*p-sa);sgb=MIN(MAXIF,*p+sb);stot=0;sum=0.0f;for(m=sga;m<=sgb;m++){sum+=(m*histo[m]);stot+=histo[m];};*ptemp=(float)sum/(float)stot;'
rankfiltercore['spectral_median'] = 'sga=MAX(0,*p-sa);sgb=MIN(MAXIF,*p+sb);stot=0;sum=0;for(m=sga;m<=sgb;m++){stot+=histo[m];};for(m=sga;m<=sgb;m++){sum+=(float)histo[m];if(sum>=(float)stot*.5){*ptemp=m;m=HISTOSIZE;}};'
rankfiltercore['spectral_gradient'] = 'sga=MAX(0,*p-sa);sgb=MIN(MAXIF,*p+sb);for(m=sgb+1;(--m>=sga)&&(histo[m]==0);); max=m; for(m=sga;(m<=sgb)&&(histo[m]==0);m++); min=m; *ptemp=max-min;'
rankfiltercore['spectral_morph_contr_enh'] = 'sga=MAX(0,*p-sa);sgb=MIN(MAXIF,*p+sb);for(m=sgb;(--m>=sga)&&(histo[m]==0);); max=m; for(m=sga;(m<=sgb)&&(histo[m]==0);m++); min=m; if(abs(*p-min)<abs(*p-max)) *ptemp=min; else *ptemp=max;'
rankfiltercore['spectral_minimum'] = 'sga=MAX(0,*p-sa);sgb=MIN(MAXIF,*p+sb);for(m=sga;(m<=sgb)&&(histo[m]==0);m++);*ptemp=m;'
rankfiltercore['spectral_maximum'] = 'sga=MAX(0,*p-sa);sgb=MIN(MAXIF,*p+sb);for(m=sgb+1;(--m>=sga)&&(histo[m]==0););*ptemp=m;'
rankfiltercore['spectral_volume'] = 'sga=MAX(0,*p-sa);sgb=MIN(MAXIF,*p+sb);stot=0;for(m=sga;m<=sgb;m++){stot+=histo[m];};*ptemp=255*(float)stot/(float)pop;'
rankfiltercore['spectral_autolevel'] = 'sga=MAX(0,*p-sa);sgb=MIN(MAXIF,*p+sb);for(m=sgb+1;(--m>=sga)&&(histo[m]==0);); max=m; for(m=sga;(m<=sgb)&&(histo[m]==0);m++); min=m; if(max-min){*ptemp=((float)(*p-min)/(float)(max-min)*MAXIF);}else{*ptemp=(float)*p;}'
rankfiltercore['spectral_lowest'] = 'sga=MAX(0,*p-sa);sgb=MIN(MAXIF,*p+sb);for(m=sga;(m<=sgb)&&(histo[m]==0);m++); min=m; if(abs(*p-min)<=delta)*ptemp=MAXI;'
rankfiltercore['spectral_highest'] = 'sga=MAX(0,*p-sa);sgb=MIN(MAXIF,*p+sb);for(m=sgb+1;(--m>=sga)&&(histo[m]==0);); max=m; if(abs(*p-max)<=delta)*ptemp=MAXI;'
#spectral soft kernels
rankfiltercore['spectral_soft_autolevel'] = 'sga=MAX(0,*p-sa);sgb=MIN(MAXIF,*p+sb);for(m=sgb+1;--m&&(histo[m]==0);); max=m; for(m=sga;(m<=sgb)&&(histo[m]==0);m++); min=m;if(max-min){f=((float)(*p-min)*MAXIF)/(float)(max-min);    f=MAX(0,MIN(MAXIF,f)); *ptemp=f;}else{*ptemp=(float)*p;}'
rankfiltercore['spectral_soft_morph_contr_enh'] = 'sga=MAX(0,*p-sa);sgb=MIN(MAXIF,*p+sb);stot=0;for(m=sga;m<=sgb;m++){stot+=histo[m];};sum=0;for(m=sga;m<=sgb;m++){sum+=histo[m];if(sum>(float)stot*inf){min=m; m=HISTOSIZE;}}; sum=0;for(m=sga;m<=sgb;m++){sum+=histo[m];if(sum>(float)stot*sup){max=m; m=HISTOSIZE;}} if(abs(*p-min)<abs(*p-max)) *ptemp=min;    else *ptemp=max;'
def kernelList():
    """
    :param: no param   
    :returns: list of the available kernel for the rank filter function
    :rtype: list of stings

    returns the available kernel for the rank filter function

    example:

    >>> from rankfilter import kernelList
    """
    list = [fname for fname in rankfiltercore]
    list.sort()
    return list


def rankfilter(ima,filtName = 'mean',radius = 3,infSup = (.1,.9), mask = None,bitDepth = 8,verbose = False,spectral_interval = (5,5)):
    """compute the rank filtered 2D image on a radius wide circular window
    could eventually be limited on a given mask
    border of the image are filtered too
    the list of the available filters is given using kernelList()
        
    :param ima: image array
    :type ima: uint8 or uint16 numpy.ndarray
    :param filtName: string name of the rankfilter as given by kernelList()  
    :type filtName: string
    :param bitDepth: number of bits used max 16  
    :type bitDepth: int[1-16]
    :param radius: radius of the neighborhood used 
    :type radius: float [1-100]
    :param infSup: inf and sup value used for 'soft' filters e.g. (.01,.99)
    :type infSup: float tuple (,)
    :param mask: mask image (if None : complete image area is filtered)
    :type mask: uint8 or uint16 numpy.ndarray
    :param verbose: True : displays details during C code executes (default is False)
    :type verbose: Bool
    :param spectral_interval: if (a,b) the interval [g-a,g+b] is used to limit the neighborhood to similar points (w.r.t. gray level g) this option is only used with 'spectral' kernels (e.g. spectral_mean)
    :type infSup: float tuple (,)

    :returns:  filtered image (same size and type as the original image)
    :raises: ValueError

    
    example:

    >>> from scipy import misc
    >>> from rankfilter import rankfilter
    >>> im = misc.imread('../../test/data/cameraman.tif')    
    >>> f = rankfilter(im,filtName = 'mean',radius = 30,verbose = False)
    >>> misc.imsave('../../test/temp/cameraman_filt_mean.tif',f)
    >>> print f
    [[158 158 159 ..., 155 154 154]
     [158 159 159 ..., 155 154 154]
     [158 159 159 ..., 155 154 154]
     ..., 
     [125 125 125 ..., 113 113 114]
     [125 125 125 ..., 114 114 114]
     [124 125 125 ..., 114 114 114]]
    
    """
    
    if not isinstance(ima,npy.ndarray):
        raise TypeError('2D numpy.array expected')
    if not ( (ima.dtype == npy.uint8) | (ima.dtype == npy.uint16)):
        raise TypeError('uint8 or uint16 numpy.array expected')
    if not(len(ima.shape) == 2):
        raise TypeError('2D numpy.array expected')
    if not  rankfiltercore.has_key(filtName):
        raise ValueError('unknown filter name %s. Use kernelList() for a complete list.'%(filtName))
        
    DATATYPE =  _dtype2ctype(ima)
    MAXRADIUS = 100
    
    #parameters
    if  ((radius > MAXRADIUS) | (radius<1)) :
        raise ValueError('radius must be [0,%d]'%MAXRADIUS)
    
    (infRange,supRange) = infSup
    (infSpectralRange,supSpectralRange) = spectral_interval
    
    if ima.dtype == npy.uint8:
        HISTOSIZE = 256
        MAXI = 255
        MEDI = 127
        MAXIF = '255.0f'
        MEDIF = '127.0f'
    else:
        if bitDepth not in range(0,17):
            raise ValueError('bitDepth must be in [0,16] range')
        HISTOSIZE = 2**bitDepth
        MAXI = HISTOSIZE-1
        MEDI = HISTOSIZE/2 - 1
        MAXIF = '%d.0f'%MAXI
        MEDIF = '%d.0f'%MEDI
        ima = npy.minimum(ima,HISTOSIZE-1) #avoid value >10bit

    #extend image border and create mask
    (m,n) = ima.shape
    extIn   = npy.zeros((m+2*radius, n+2*radius),dtype = ima.dtype, order='C')
    extMask = npy.zeros((m+2*radius, n+2*radius),dtype = ima.dtype, order='C')
    extOut  = npy.zeros((m+2*radius, n+2*radius),dtype = ima.dtype, order='C')
    extIn[radius:m+radius,radius:n+radius] = ima
    
    if mask is not None:
        if not isinstance(mask,npy.ndarray):
            raise TypeError('2D numpy.array expected for mask')
        if not( ima.shape == mask.shape):
            raise ValueError('ima(%dx%d) and mask(%dx%d) must have same sizes.'%(ima.shape[0],ima.shape[1],mask.shape[0],mask.shape[1]))
        extMask[radius:m+radius,radius:n+radius] = mask
    else: #default mask covers the entire image
        extMask[radius:m+radius,radius:n+radius] = 1
    
    #activate or de-activate verbose in C code
    if verbose:
        VERBOSE = 'True'
    else:
        VERBOSE = ''
        
    #code contains the main circular window displacement loop
    #specific rank filter function is injected via the _PROCESSING_ preprocessing directive
    code = \
"""
#define VERBOSE %s
#define DATATYPE %s
#define HISTOSIZE %d
#define HISTOSIZE1 %d
#define MAXI %d
#define MEDI %d
#define MAXIF %s
#define MEDIF %s
#define MAXRADIUS %d
#define _PROCESSING_ %s

#define MIN(a,b) (a>b?(b):(a))
#define MAX(a,b) (a<b?(b):(a))

DATATYPE *IN = (DATATYPE *) PyArray_GETPTR1(extIn_array,0);
DATATYPE *MASK = (DATATYPE *) PyArray_GETPTR1(extMask_array,0);
DATATYPE *OUT = (DATATYPE *) PyArray_GETPTR1(extOut_array,0);
int N_IN = PyArray_SIZE(extIn_array);
int m_IN = PyArray_DIM(extIn_array,0);
int n_IN = PyArray_DIM(extIn_array,1);
int size = radius;
int inumx = n_IN;
int inumy = m_IN;
int min,max,pmax,count;
float f;
float rank = infRange;
float inf = infRange;
float sup = supRange;
float delta = infRange;
int sa = infSpectralRange;
int sb = supSpectralRange;
int sga,sgb,stot;

int histo[HISTOSIZE];
DATATYPE *p0,*p1,*p2,*p3,*p4,*p5,*p,*ptemp;
int i,j,k,l,m;
long int t;
float pop,sum;
int size2;
int dcn[MAXRADIUS*2+1];
int dcs[MAXRADIUS*2+1];
int dce[MAXRADIUS*2+1];
int dco[MAXRADIUS*2+1];
int d;

size2=(int)(size*2.0f+1.0f);
for(i=0;i<(int)size+1;i++){
    d=(int)(sqrt((double)(size*size)-(double)(i*i)));
    dco[((int)size)-i]= - d + (((int)size)-i)*inumx;
    dco[((int)size)+i]= - d + (((int)size)+i)*inumx;
    dce[((int)size)-i]= + d + (((int)size)-i)*inumx;
    dce[((int)size)+i]= + d + (((int)size)+i)*inumx;
    dcn[((int)size)-i]= - d*inumx + (((int)size)-i);
    dcn[((int)size)+i]= - d*inumx + (((int)size)+i);
    dcs[((int)size)-i]= + d*inumx + (((int)size)-i);
    dcs[((int)size)+i]= + d*inumx + (((int)size)+i);}
p=IN;
ptemp=OUT;
t=MASK-IN;
for(i=0;i<HISTOSIZE;i++)histo[i]=0;
pop=0.0f;
p0=p+(int)size;
for(j=0;j<size2;j++){
    for(i=0;i<size2;i++){
        d=ceil(sqrt((size-(float)i)*(size-(float)i)+(size-(float)j)*(size-(float)j)));
        if(d<=(int)size){
        if(*(p+t+i+j*inumx)){
        histo[*(p+i+j*inumx)]++;
        pop++;}}}}
p1=p0+1;
p2=p0-1;
p3=p0;
p4=p+(int)size*inumx;
p5=p4+inumx;
p+=((int)size+(int)size*inumx);
ptemp+=((int)size+(int)size*inumx);
j=size;
l=inumx-2*size;
for(;;){
    for(i=l;--i;){
        _PROCESSING_
        for(k=0;k<size2;k++){
        if(*(p1+dce[k]+t)){histo[*(p1+dce[k])]++;pop++;}
        if(*(p0+dco[k]+t)){histo[*(p0+dco[k])]--;pop--;}}
        p++;ptemp++;p0++;p1++;p2++;p3++;p4++;p5++;}
    _PROCESSING_
    j++;
    if(j>=inumy-size) break;
    for(k=0;k<size2;k++){
    if(*(p5+dcs[k]+t)){histo[*(p5+dcs[k])]++;pop++;}
    if(*(p4+dcn[k]+t)){histo[*(p4+dcn[k])]--;pop--;}}
    p+=inumx;ptemp+=inumx;p0+=inumx;p1+=inumx;p2+=inumx;p3+=inumx;p4+=inumx;p5+=inumx;
    for(i=l;--i;){
        _PROCESSING_
        for(k=0;k<size2;k++){
        if(*(p2+dco[k]+t)){histo[*(p2+dco[k])]++;pop++;}
        if(*(p3+dce[k]+t)){histo[*(p3+dce[k])]--;pop--;}}
        p--;ptemp--;p0--;p1--;p2--;p3--;p4--;p5--;}
    _PROCESSING_
    j++;
    if(j>=inumy-size) break;
    for(k=0;k<size2;k++){
    if(*(p5+dcs[k]+t)){histo[*(p5+dcs[k])]++;pop++;}
    if(*(p4+dcn[k]+t)){histo[*(p4+dcn[k])]--;pop--;}}
    p+=inumx;ptemp+=inumx;p0+=inumx;p1+=inumx;p2+=inumx;p3+=inumx;p4+=inumx;p5+=inumx;}
    ptemp=MASK;
"""% (VERBOSE,DATATYPE,HISTOSIZE,HISTOSIZE-1,MAXI,MEDI,MAXIF,MEDIF,MAXRADIUS,rankfiltercore[filtName])
    if verbose:
        print 'C code [%s] ="%s"\nprocess = "%s"'%(filtName,code,rankfiltercore[filtName])
        
    inline(code, ['extIn','extMask','extOut','radius','infRange','supRange','infSpectralRange','supSpectralRange'])

    return extOut[radius:m+radius,radius:n+radius]

def benchmark():
    import datetime
    
    im = misc.imread('../../test/data/cameraman.tif')
    fname = 'rank'    
    start = datetime.datetime.now()
    f = rankfilter(im,filtName = fname,radius = 3,verbose = False)
    timedelta = datetime.datetime.now()-start
    print 'enlapsed time for one filter : %d microsecond'%timedelta.microseconds
    
    start = datetime.datetime.now()
    for i in range(10):
        f = rankfilter(im,filtName = fname,radius = 3,verbose = False)
    timedelta = datetime.datetime.now()-start
    print 'enlapsed time for 10 filters : %d microsecond (%f µsec each)'%(timedelta.microseconds,timedelta.microseconds/10.0)

def testAll():
    #Apply all the available rankfilter to a single cameraman.tif image (radius = 20 and default params)
#    im = misc.imread('../../test/data/cameraman.tif')
    im = misc.imread('../../test/data/testpng.png')
    im = im[:,:,0]
    print 'test all'
    for fname in rankfiltercore :
        print 'test: %s'%fname
        f = rankfilter(im,filtName = fname,radius = 15,verbose = False,infSup=(.1,.9))
        misc.imsave('../../test/temp/cameraman_filt_%s.tif'%fname,f)

def testAllSpectral():
    #Apply all the available rankfilter to a single cameraman.tif image (radius = 20 and default params)
#    im = misc.imread('../../test/data/exp0012.jpg')
#    im = misc.imread('../../test/data/exp0001.jpg')
#    im = misc.imread('../../test/data/cameraman.tif')
    im = misc.imread('../../test/data/testpng.png')
    im = im[:,:,0]
    print 'test one'
    fnames = [name for name in rankfiltercore if 'spectral' in name]
#    in 'spectral_median','spectral_mean','morph_contr_enh','spectral_morph_contr_enh','spectral_maximum','spectral_minimum']
    for fname in fnames:
        print 'test: %s'%fname
#        m = rankfilter(im,filtName = 'median',radius = 2,verbose = False,infSup=(.1,.9),spectral_interval=(10,10))
        f = rankfilter(im,filtName = fname,radius = 10,verbose = False,infSup=(.1,.9),spectral_interval=(10,10))
        misc.imsave('../../test/temp/cameraman_filt_%s.tif'%fname,f)

def testMask():
    """Open a 8 bit image, apply the filter on an image part given by a mask
    the mask pproperty allo to limit the application filter on the mask area, the rest of the image 
    is not valid(i.e. is set to 0 outside the dilated mask, pixels situated in the radius distance from the mask should not be used)"""    
    im = misc.imread('../../test/data/cameraman.tif')    
    fname = 'soft_autolevel'
    mask_image = im<40
    print mask_image.dtype
    f = rankfilter(im,filtName = fname,radius = 10,verbose = False, mask = mask_image)
    result = im
    result[mask_image] = f[mask_image]
    misc.imsave('../../test/temp/cameramanMask_filt_%s.tif'%fname,result)

def testUint16():
    #Open a 8 bit image, convert it into uint16 and apply the 11 bit version of the rank filter 
    im = misc.imread('../../test/data/cameraman.tif')    
    fname = 'mean' 
    f = rankfilter(im.astype('uint16'),bitDepth=11,filtName = fname,radius = 3,verbose = False)
    print f.dtype
    print f
    misc.imsave('../../test/temp/cameraman16_filt_%s.tif'%fname,f)

def testBitDepth():
    #Open a 8 bit image, apply rank filter using different bitdepth ! im type must be uint16 
    im = misc.imread('../../test/data/cameraman.tif')    
    fname = 'mean' 
    f = rankfilter(im.astype('uint16'),bitDepth=5,filtName = fname,radius = 3,verbose = False)
    print f.dtype
    print f
    misc.imsave('../../test/temp/cameramanDepth_filt_%s.tif'%fname,f)    
    
if __name__ == "__main__":
    #automatic testing with doctest module see >>> lines in the function doc
    import doctest
#    doctest.testmod()
    #read filter and save an image
    
    print help(rankfilter)

    testAll()
#    testBitDepth()
#    testMask()

    testAllSpectral()

    #example of profiling using cprofile
    import cProfile
    cProfile.run('benchmark()','../../test/temp/fooprof')
    import pstats
    p = pstats.Stats('../../test/temp/fooprof')
    p.strip_dirs().sort_stats('cumulative').print_stats()
    
    klist = kernelList()
    klist.sort()
    for k in klist:
        print k
        