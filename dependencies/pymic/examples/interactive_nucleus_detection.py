__author__ = 'olivier'

import struct
import numpy as np
import os

from scipy.ndimage import measurements
from scipy.fftpack import fft, ifft, fftshift, ifftshift
from scipy.ndimage.filters import gaussian_filter
from scipy.ndimage import measurements

import matplotlib.cm as cm
import matplotlib.pyplot as plt

from enthought.traits.api import Range
from enthought.traits.ui.api import Item, View, VSplit, HSplit, VGroup

import sys
sys.path.append('../')
from rankfilter import rankfilter
from watershed import watershed
from zvi import zviread
from uitools.interactive import Process, Tester


def egal(ima,dtype='uint16',n = 1024):
    fima = ima.flatten()
    print max(fima)+1
    h,bins = np.histogram(fima,bins = range(0,max(fima)+2))
    ch = np.cumsum(h)
    lut = n*ch/ch[-1]
    return (lut[ima]).astype(dtype)

def autolevel(ima,dtype='uint16',n = 1024):
    fima = ima.flatten()
    m = min(fima)
    M = max(fima)
    delta = float(M-m)/n
    res = ((ima-m)/delta).astype(dtype)
    return res

def remove_small_obj(bw, area=10000):
    labels, numfeat = measurements.label(bw)
    area_obj = measurements.sum(bw, labels, index=range(numfeat + 1))
    lut = area_obj > area
    result = lut[labels]
    return result

def remove_small_holes(bw, area = 50000):
    labels, numfeat = measurements.label(bw == 0)
    area_obj = measurements.sum(bw==0, labels, index=range(numfeat + 1))
    lut = area_obj > 50000
    result = lut[labels]==0
    return result

class BgDetectionProcess(Process):
    radius = Range(1.0,20,10)
    threshold = Range(0.0,300,50)
    traits_view = View(VGroup(Item('radius'),
                    Item('threshold')))
    def __init__(self):
        #add here some initialisation if needed
        pass

    def apply(self,input):
        #here is the code that return an image from the input image
        al = autolevel(input)
        gr = rankfilter(al,'gradient',self.radius,bitDepth=10)
        #keep region with few borders
        thresh = gr<self.threshold
        #remove small foreground objects
        bg = remove_small_obj(thresh,10000)
        #remove small holes in background objects
        bg = remove_small_holes(bg,50000)
        return bg

class NucleusDetectionProcess(Process):
    radius = Range(1.0,20,10)
    threshold = Range(0.0,2048,1024)
    traits_view = View(VGroup(Item('radius'),
                    Item('threshold')))
    def __init__(self,input):
        #add here some initialisation if needed
        self.data = autolevel(input).astype('uint16')

    def apply(self):
        #here is the code that return an image from the input image

#        gr = rankfilter(al,'gradient',self.radius,bitDepth=10)
        #keep region with few borders
#        thresh = gr<self.threshold
        #remove small foreground objects
#        bg = remove_small_obj(thresh,10000)
        #remove small holes in background objects
#        bg = remove_small_holes(bg,50000)
        bg = self.data>self.threshold
        return bg


if __name__ == "__main__":
    dir = ['../test/data/'+f for f in os.listdir('../test/data/') if 'zvi' in f]
    print dir
    dir = [dir[0]]
    color = [cm.gray,cm.Blues,cm.Greens,cm.Oranges]


    for i,fname in enumerate(dir):
        dic = zviread(fname,0).Image.Array
        dapi = zviread(fname,1).Image.Array

#        tester = Tester(process,dic.astype('float'))
        process = NucleusDetectionProcess(dapi.astype('float'))
        tester = Tester(process)
        tester.configure_traits()
        print tester.output
#        bg = background_detection(dic)
#
#
#        plt.figure(i)
#        plt.subplot(1,2,1)
#        plt.imshow(dic,origin='lower',cmap=color[0],interpolation='nearest')
#        plt.colorbar()
#        plt.subplot(1,2,2)
#        plt.imshow(bg,origin='lower',cmap=color[0],interpolation='nearest')
#        plt.colorbar()
    plt.show()

