# -*- coding: utf-8 -*-
'''
Test the scipy weave module to the watershed transform
use Weave for C compilation

.. code-block:: python

   from watershed import watershed
   im = scipy.misc.imread('../../test/data/cameraman.tif')
   [label,borders,nlabel]  = watershed(im)
   scipy.misc.imsave('../../test/temp/watershedBorders.tif',f)
    
'''
__author__ = 'olivier'

#Stuff related to the inline compilation required to return a value with weave.inline
import sys, os
support_dir = os.path.normpath(os.path.join(sys.prefix,'share','python%d.%d' % (sys.version_info[0],sys.version_info[1]),'CXX') )
#cxx_sources = [os.path.join(support_dir, f) for f in ["cxxextensions.c", "cxx_extensions.cxx", "IndirectPythonInterface.cxx", "cxxsupport.cxx"]]

import numpy as npy
from scipy.weave import inline
import scipy



def dtype2ctype(array):
    """convert numpy type in C equivalent type
    """
    types = {
        npy.dtype(npy.float64): 'double',
        npy.dtype(npy.float32): 'float',
        npy.dtype(npy.int32):   'int',
        npy.dtype(npy.uint32):  'unsigned int',
        npy.dtype(npy.int16):   'short',
        npy.dtype(npy.uint8):   'unsigned char',
        npy.dtype(npy.uint16):  'unsigned short',
    }
    return types.get(array.dtype)

watershedMode = ['unmarked','marked','modifiedGradient']
                   
def watershed(ima,mode = 'unmarked', markers = None,verbose = False):
    """compute the watershed transform on a 8 bit unsigned 2D aray
    could eventually use a markers image

    :param ima: image array
    :type ima: uint8
    :param type: 'unmarked','marked','modifiedGradient'
    :type type: string
    :param markers: markers image (marks are 255)
    :type markers: uint8
    :param verbose: True : displays details during C code executes (default is False)
    :type verbose: Bool

    :returns:  (modes 'unmarked','marked')
    :returns:  (modes 'modifiedGradient')

    :raises: TypeError

    example:

    >>> from watershed import watershed
    >>> im = scipy.misc.imread('../../test/data/cameraman.tif')
    >>> [label,borders,nlabel]  = watershed(im)
    >>> scipy.misc.imsave('../../test/temp/watershedBorders.tif',borders)
    """
    
    if not isinstance(ima,npy.ndarray):
        raise TypeError('2D numpy.array expected')
    if not (ima.dtype == npy.uint8) :
        raise TypeError('uint8 numpy.array expected')
    if not(len(ima.shape) == 2):
        raise TypeError('2D numpy.array expected')
    if not  mode in watershedMode:
        raise ValueError('unknown type %s. "unmarked","marked","modifiedGradient".'%(mode))
        
    #create result images/copy ima instance
    
    (m,n) = ima.shape 
    labels  = npy.zeros((m,n),dtype = npy.uint32, order='C')
    ima = ima.copy() #because do_water function modifies the input image
        
    if markers is not None:
        if not isinstance(markers,npy.ndarray):
            raise TypeError('2D numpy.array expected for markers')
        if not( ima.shape == markers.shape):
            raise ValueError('ima(%dx%d) and markers(%dx%d) must have same sizes.'%(ima.shape[0],ima.shape[1],markers.shape[0],markers.shape[1]))
        if not (markers.dtype == npy.uint8) :
            raise TypeError('uint8 numpy.array expected for markers')
    
    if mode == 'unmarked':
        imode = 1        
        if markers is None:
            markers = npy.zeros((1,1),dtype = npy.uint8, order='C') #needed for parameters purposes in C code
        
    if mode == 'marked':
        imode = 2
        if markers is None:
            raise ValueError('need a markers image in mode marked')
        
    if mode == 'modifiedGradient':
        imode = 3
        if markers is None:
            raise ValueError('need a markers image in mode modifiedGradient')
            
    #activate or de-activate verbose in C code
    if verbose:
        VERBOSE = '#define VERBOSE True'
    else:
        VERBOSE = '#undef VERBOSE'
        
    #code contains the main watershed C code
    code = \
"""
%s
unsigned char *IN      = (unsigned char *) PyArray_GETPTR1(ima_array,0);
unsigned char *MARKERS = (unsigned char *) PyArray_GETPTR1(markers_array,0);
unsigned int  *LABELS  = (unsigned int *) PyArray_GETPTR1(labels_array,0);
int n;

#ifdef  VERBOSE    
    printf("(watershed mode: %%d)\\n",imode);
#endif

int N_IN = PyArray_SIZE(ima_array);
int m_IN = PyArray_DIM(ima_array,0); 
int n_IN = PyArray_DIM(ima_array,1);
int sizex = n_IN;
int sizey = m_IN;

// call the function defined in the watershed.c file

n = do_water(imode,IN,MARKERS,LABELS,sizex,sizey);

//in order to avoid problem with compiling using Cxx extension, watershed function will return no value
//return an int to Python
//return_val = Py::new_reference_to(Py::Int(n));

"""% (VERBOSE)

    extra_code = open(os.path.join(os.path.split(__file__)[0],'watershed.c')).read()
    
    #in order to avoid problem with compiling using Cxx extension, watershed function will return no value
    #nlabel = inline(code, ['ima','markers','labels','imode'],support_code=extra_code,headers=["<CXX/Objects.hxx>", "<CXX/Exception.hxx>"], sources=cxx_sources)
    inline(code, ['ima','markers','labels','imode'],support_code=extra_code)
    nlabel = npy.max(labels) 
    if mode == 'modifiedGradient':
        return ima
    else:
        return (labels,ima,nlabel)

def test_markers():
    im = scipy.misc.imread('../../test/data/cameraman.tif')
    grad = rankfilter(im,'gradient',5)    
    f = ((grad<50)*255).astype('uint8')
    scipy.misc.imsave('../../test/temp/watershedMarkers.tif',f)
    [label,borders,nlabel]  = watershed(grad,markers = f,mode = 'marked')
    print nlabel
    scipy.misc.imsave('../../test/temp/watershedBorders.tif',borders)
    scipy.misc.imsave('../../test/temp/watershedLabels.tif',label)
    [label,borders,nlabel]  = watershed(grad,markers = f,mode = 'unmarked')
    print nlabel
    scipy.misc.imsave('../../test/temp/watershedBordersUnmarked.tif',borders)
    scipy.misc.imsave('../../test/temp/watershedLabelsUnmarked.tif',label)
    modif  = watershed(grad,markers = f,mode = 'modifiedGradient')
    scipy.misc.imsave('../../test/temp/watershedModifGradient.tif',modif)
    
def testBig():
    print 'may take a while (5984x6168) ...'
    im = scipy.misc.imread('../../test/data/iko_interp8bit.tif')
    grad = rankfilter(im,'gradient',5)
    print '... gradient ok ...'
    mark = ((grad<5)*255).astype('uint8')
    [label,borders,nlabel]  = watershed(grad,mode = 'marked',markers=mark)
    scipy.misc.imsave('../../test/temp/bigWatershed.tif',borders)
    print '... done !'
    
def test():
    '''test watershed functions on a synthetic image

        example:
    
    >>> from watershed import test
    >>> test()
    [[1 1 1 1 1 1 1 2 2 2]
     [1 1 1 1 1 1 2 2 2 2]
     [1 1 1 1 1 1 2 2 2 2]
     [1 1 1 1 1 1 2 2 2 2]
     [1 1 1 1 1 2 2 2 2 2]
     [1 1 1 1 1 2 2 2 2 2]
     [1 1 1 1 1 2 2 2 2 2]
     [1 1 1 1 2 2 2 2 2 2]
     [1 1 1 2 2 2 2 2 2 2]
     [1 1 2 2 2 2 2 2 2 2]]
    [[1 1 1 1 1 1 1 1 1 3]
     [1 1 1 1 1 1 1 1 3 3]
     [1 1 1 1 1 1 1 3 3 3]
     [1 1 1 1 1 1 3 3 3 3]
     [2 2 2 2 2 2 3 3 3 3]
     [2 2 2 2 2 2 3 3 3 3]
     [2 2 2 2 2 3 3 3 3 3]
     [2 2 2 2 3 3 3 3 3 3]
     [2 2 2 2 3 3 3 3 3 3]
     [2 2 2 2 3 3 3 3 3 3]]
    
    '''
    X, Y = npy.meshgrid(range(10), range(10))
    Z1 = npy.sqrt((X-3)**2+(Y-3)**2)
    Z2 = npy.sqrt((X-7)**2+(Y-5)**2)
    Z = npy.minimum(Z1,Z2) * 25.0
    im = Z.astype('uint8')
    [label,borders,nlabel] = watershed(im,mode = 'unmarked')
    print label
    scipy.misc.imsave('../../test/temp/test1.png',im)
    scipy.misc.imsave('../../test/temp/test2.png',label)
    mark = npy.zeros(im.shape,'uint8')
    mark[2,2] = 1
    mark[5,2] = 1
    mark[7,7] = 1
    [label,borders,nlabel]  = watershed(im,mode = 'marked',markers = mark)
    scipy.misc.imsave('../../test/temp/test3.png',label)
    print label
    
if __name__ == "__main__":

    #Import dependencies needed for test functions (e.g. using rankfilter...)
    sys.path.append('..')
    from rankfilter import rankfilter

    import doctest
    doctest.testmod()
    
    test()     
    test_markers()
#    testBig()
    help(watershed)
    