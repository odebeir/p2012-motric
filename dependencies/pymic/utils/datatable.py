# -*- coding: utf-8 -*-
'''
Simple classes for data table handling

.. code-block:: python

   from utils.datatable import Table
   table = Table([('f0','n.a.'),('f1',-999.0),('f2',99.0)])
   for i in range(5):
       r = table.add_row(f0='azer%d'%i)
       r.f1 = -i*100
       r.f2 = i*10
       r.f3 = 888
   print table.to_csv_string()

'''
__author__ = "Olivier Debeir"
__copyright__ = "Copyright 2011, LISA ULB"
__license__ = "???"
__email__ = "odebeir@ulb.ac.be"

from collections import OrderedDict
import numpy

class Header(object):
    """Row object maintains the row structure(columns) and allows the dot notation
    """
    def __init__(self,columns):
        self.__dict__['_columns'] = columns
        c = []
        self.__dict__['_features'] = []
        for k,value in columns:
            self.__dict__[k] = value
            self._features.append(k)
            if type(value) is not str:
                c.append((k,value))
        self.__dict__['_numeric_only'] = c

    def set(self,**kwargs):
        for k in kwargs:
            self.__dict__[k] = kwargs[k]
#            self.__setattr__(k,kwargs[k])

    def __setattr__(self, key, value):
        if key in self._features:
            self.__dict__[key] = value
        else:
            print 'Feature %s(%s) not declared in Table header'%(key,str(value))

    def get_n(self):
        return len(self._columns)

class Table(object):
    """Table maintains the list of Rows, and offers export facilities
    """
    def __init__(self,columns):
        self.header = Header(columns)
        self.data = []

    def add_row(self,**kwargs):
        new_row = Header(self.header._columns)
        new_row.set(**kwargs)
        self.data.append(new_row)
        return self.data[-1]

    def to_array(self):
        '''returns an array from numerical columns'''
        array = numpy.zeros((len(self.data),len(self.header._numeric_only)))
        print 'num',self.header._numeric_only

        for r,row in enumerate(self.data):
            values = [row.__dict__.get(k,default) for k,default in self.header._numeric_only]
            array[r,:] = numpy.asarray(values)
        return array

    def to_csv_string(self,header=True,default=True):
        """export Table to a CSV string
        
        :param header: if True, header are exported in the first line
        :type header: bool
        :param default: if True and header are displayed, default values are mentioned in [ ] beside columns names
        :type header: bool

        :returns:  CSV string

        example:

        >>> table = Table([('f0','n.a.'),('f1',-999.0),('f2',99.0)])
        >>> r = table.add_row(f0='azerty')
        >>> r.f1 = 100
        >>> r.f2 = 10
        >>> r = table.add_row(f0='azerty')
        >>> r.f1 = -569
        >>> r.f2 = 1089

        >>> r.f3 = 888
        Feature f3(888) not declared in Table header

        >>> print table.to_csv_string()
        f0[n.a.],f1[-999.0],f2[99.0]
        azerty,100,10
        azerty,-569,1089
        <BLANKLINE>
        """
        if header:
            if default:
                s = ','.join(['%s[%s]'%k for k in self.header._columns])+'\n'
            else:
                s = ','.join(['%s'%k[0] for k in self.header._columns])+'\n'
        else:
            s = ''

        for row in self.data:
            s = s + ','.join([str(row.__dict__.get(k,default)) for k,default in self.header._columns])+'\n'
        return s

    def dump(self,column_width=15):
        """dump Table to a text formated string

        :param column_width: default is 15 (text is left cropped)
        :type header: int

        :returns:  None
        
        >>> table = Table([('f0','n.a.'),('f1',-999.0),('f2',99.0)])
        >>> for i in range(5):
        ...     r = table.add_row(f0='azer%d'%i)
        ...     r.f1 = -i*100
        ...     r.f2 = i*10
        >>> table.dump()
        + ----------------------------------------------- +
        |              f0|             f1|             f2 |
        + ----------------------------------------------- +
        |           azer0|              0|              0 |
        |           azer1|           -100|             10 |
        |           azer2|           -200|             20 |
        |           azer3|           -300|             30 |
        |           azer4|           -400|             40 |
        + ----------------------------------------------- +
        """
        fmt = '%'+str(column_width)+'s'
        n = self.header.get_n()
        print '+','-'*(n*(column_width+1)-1),'+'
        print '|','|'.join([fmt%k[0][-column_width:] for k in self.header._columns]),'|'
        print '+','-'*(n*(column_width+1)-1),'+'
        for row in self.data:
            print '|','|'.join([fmt%str(row.__dict__.get(k,default))[-column_width:] for k,default in self.header._columns]),'|'
        print '+','-'*(n*(column_width+1)-1),'+'


if __name__ == "__main__":

    import doctest
    doctest.testmod()

    table = Table([('f0','n.a.'),('f1',-999.0),('f2',99.0)])
    for i in range(5):
        r = table.add_row(f0='azer%d'%i)
        r.f1 = -i*100
        r.f52 = i*10
        r.f3 = 888

    print table.to_csv_string()
    print table.to_csv_string(header=False)
    print table.to_csv_string(default=False)

    array = table.to_array()
    print array
    table.dump()
  