import sys
sys.path.append('../dependencies')

import os
import matplotlib.pyplot as plt

data_path = '../data/jpeg2'

fnames = os.listdir(data_path)

n = len(fnames)-1 # ignore text file

for i in range(n):
    filepath = os.path.join(data_path,'ima%04d.jpg'%i)
    data = plt.imread(filepath)
    plt.figure()
    plt.imshow(data)
    plt.show()


